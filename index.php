<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet">
    <title>login</title>
</head>

<body>
    <?php
    session_start();
    if (empty($_SESSION["mail"])) {

        header("Location:login.php");
    }
    ?>
    <?php include('header.php'); ?>
    <main>
        <h2>Bravo vous êtes connecter!<?php echo $_SESSION["mail"] ?></h2>
        <a class=" btn" href="login.php?logout=1">Se déconnecter</a>
    </main>

    <?php include('footer.php'); ?>
</body>

</html>